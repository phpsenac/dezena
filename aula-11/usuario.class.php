<?php

class Usuario{

    private $id;
    private $nome;
    private $email;
    private $senha;

    public function __construct(){
        
        echo "Aqui será feita a conexão com o SGDB";
    }

    public function getId(int $id): int{
        return $this->$id;
    }
    public function setId(int $id){
        $this->$id = $id;
    }
    public function getNome(string $nome): string{
        return $this->$nome;
    }
    public function setNome(string $nome){
        $this->$nome = $nome;
    }
    public function getEmail(string $email): string{
        return $this->$email;
    }
    public function setEmail(string $email){
        $this->$email = $email;
    }
    public function getSenha(string $senha): string{
        return $this->$senha;
    }
    public function setSenha(string $senha){
        $this->$senha = $senha;
    }

    public function __destruct(){
        echo "<br>Fechando a conexão com o SGBD";
    }
    
    
    
    
   

}