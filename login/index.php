

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cadastro e Login sem BD</title>
</head>

<body>

<form name="cadastro" action="#" method="post">
	<input type="email" name="cad_email" placeholder="Email..."><br>
	<input type="password" name="cad_senha" placeholder="Senha"><br>
	<input type="submit" name="cad_enviar" value="Cadastrar">
</form>

<hr><br><br>

<form name="login" action="login.php" method="post">
	<input type="email" name="log_email" placeholder="Email..."><br>
	<input type="password" name="log_senha" placeholder="Senha"><br>
	<input type="submit" name="log_enviar" value="Logar">
</form>

<br><br>
<a href="pedidos.php">Pedidos</a><br>
<a href="logout.php">Deslogar</a><br>

</body>
</html>

<?php

if (isset($_POST['cad_enviar'])) { // recebe o post do cadastro
	$cad_email = $_POST['cad_email'];
	$cad_senha = $_POST['cad_senha'];

	session_start(); // inicia e grava a sessão
	$_SESSION['email'] = $cad_email;
	$_SESSION['senha'] = $cad_senha;
	echo "<script>alert('Cadastro realizado com sucesso!');window.location.replace('index.php');</script>";
}

?>
