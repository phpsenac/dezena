<?php
if (isset($_POST['log_enviar'])) { // pega o post do login
	$log_email = $_POST['log_email'];
	$log_senha = $_POST['log_senha'];
  	session_start();
	if ($log_email == $_SESSION["email"] && ($log_senha == $_SESSION['senha'])) { // verifica os dados
      unset($_SESSION["email"]); // destroi sessões anteriores
      unset($_SESSION['senha']);
      $_SESSION['email_liberado'] = $log_email; // cria novas sessões
      $_SESSION['senha_liberada'] = $log_senha;
      echo "<script>alert('Login realizado com sucesso!');window.location.replace('index.php');</script>";
	}else{
      echo "<script>alert('Erro ao logar, tente novamente!');window.location.replace('index.php');</script>";
	}
}else {
  echo "<script>alert('Você não tem permissão para acessar essa página!');window.location.replace('index.php');</script>";
}

?>