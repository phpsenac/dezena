<?php

class Usuario
{

    private $id;
    private $nome;
    private $email;
    private $senha;
    private $objDb;

    public function __construct()
    {
        $this->objDb = new mysqli(
            'localhost',
            'root',
            '',
            'aula_php',
            3307
        );
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }
    public function setNome(string $nome)
    {
        $this->nome = $nome;
    }
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
    public function setSenha(string $senha)
    {
        $this->senha = password_hash($senha, PASSWORD_DEFAULT);
    }
    public function getId(int $id) : int
    {
        return $this->id;
    }
    public function getNome(string $nome) : string
    {
        return $this->nome;
    }
    public function getEmail(string $email) : string
    {
        return $this->email;
    }
    public function getSenha(string $senha) : string
    {
        return $this->senha;
    }
    public function saveUsuario()
    {
        $objStmt = $this->objDb->prepare('REPLACE INTO 
                                        tb_usuario
                                        (id ,nome, email,senha)
                                        values
                                        (?,?,?,?)');
        $objStmt->bind_param(
            'isss',
            $this->id,
            $this->nome,
            $this->email,
            $this->senha
        );
        if ($objStmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
    public function deletarUsuario()
    {
        $objStmt = $this->objDb->prepare("DELETE FROM tb_usuario WHERE id = ?");

        $objStmt->bind_param("i", $this->id);

        $objStmt->execute();
    }
    public function listarUsuario()
    {
        $objListar = $this->objDb->query("SELECT 
                                            id, nome, email, senha 
                                            FROM 
                                            tb_usuario 
                                          ");
        return $objListar;
    }
    public function __destruct()
    {
        unset($this->objDb);
    }







}



}