<?php
/*
Funcao pra gravar logs do sistema
*/

/*
function logger(string $str): bool {
    $fp = fopen ('log.txt','a');
    if (fwrite($fp, $str)){
        fclose($fp);
        return false;
    } else {
        fclose($fp);
        return true;
    }
}
*/

// Exemplo com mais de um paramentro


/* function logger(string $str, int $nr_linha): bool {
    $fp = fopen ('log.txt','a');
    if (fwrite($fp, $nr_linha . ': ' . $str)){
        fclose($fp);
        return false;
    } else {
        fclose($fp);
        return true;
    }
}
*/


function logger(string $str, int &$nr_linha = null): bool {
    $fp = fopen ('log.txt','a');
    if (fwrite($fp, $nr_linha . ': ' . $str)){
        fclose($fp);
        return false;
    } else {
        fclose($fp);
        return true;
    }
    $nr_linha = 10;
}